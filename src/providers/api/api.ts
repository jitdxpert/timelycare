import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Transfer, TransferObject } from '@ionic-native/transfer';

// import providers
import { State } from '../state/state';

@Injectable()
export class ApiProvider {

  // public apiUrl: string = 'http://timelycare.loc/-api';
  // public apiUrl: string = 'http://v1.timelycare.loc/-api';
  // public apiUrl: string = 'https://temp.playbook.ai/-api';
  public apiUrl: string = 'https://timelycare.org/-api';

  constructor(public http: HttpClient, public transfer: Transfer, public storage: Storage) {}

  public sendAdditionalHeaders() {
    let uid = State.get('uid');
    let token = State.get('token');
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Auth-Id', uid);
    headers = headers.set('Auth-Token', token);
    return headers;
  }

  public get(url: string, params?: any) {
    let uri = `${this.apiUrl}/${url}`;
    if(params){
      uri = `${this.apiUrl}/${url}/?${params}`;
    }
    const headers = this.sendAdditionalHeaders();
    const options = {
      headers: headers
    };
    return new Promise((resolve, reject) => {
      this.http.get(uri, options).subscribe(
        (res: any) => {
          resolve(res);
        }, (err: any) => {
          reject(err);
        }
      );
    });
  }

  public post(url: string, params: any) {
    let uri = `${this.apiUrl}/${url}`;
    const headers = this.sendAdditionalHeaders();
    const options = {
      headers: headers
    };
    return new Promise((resolve, reject) => {
      this.http.post(uri, params, options).subscribe(
        (res: any) => {
          resolve(res);
        }, (err: any) => {
          reject(err);
        }
      );
    });
  }

  public delete(url: string, id: number) {
    let uri = `${this.apiUrl}/${url}/?id=${id}`;
    const headers = this.sendAdditionalHeaders();
    const options = {
      headers: headers
    };
    return new Promise((resolve, reject) => {
      this.http.delete(uri, options).subscribe(
        (res: any) => {
          resolve(res);
        }, (err: any) => {
          reject(err);
        }
      );
    });
  }

  public update(url: string, params: any, id: number) {
    if(!params) {
      params = {};
      params.id = 0;
    }
    let uri = `${this.apiUrl}/${url}/?id=${id}`;
    const headers = this.sendAdditionalHeaders();
    const options = {
      headers: headers
    };
    return new Promise((resolve, reject) => {
      this.http.put(uri, params, options).subscribe(
        (res: any) => {
          resolve(res);
        }, (err: any) => {
          reject(err);
        }
      );
    });
  }

  public multipart(targetPath: string, url: string, options: any) {
    let uid = State.get('uid');
    let token = State.get('token');
    let uri = `${this.apiUrl}/${url}/?uid=${uid}&token=${token}`;
    const fileTransfer: TransferObject = this.transfer.create();
    return new Promise((resolve, reject) => {
      fileTransfer.upload(targetPath, uri, options).then(
        (res: any) => {
          resolve(res);
        }, (err: any) => {
          reject(err);
        }
      );
    });
  }

}
