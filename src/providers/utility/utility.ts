import { Injectable } from '@angular/core';
import { NavController, ModalController, ViewController, AlertController, ToastController, LoadingController, Loading, ActionSheetController } from 'ionic-angular';

@Injectable()
export class UtilityProvider {

  private loading: Loading;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public viewCtrl: ViewController, public alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public actionSheetCtrl: ActionSheetController){}

  public openModal(page: any, params?: any): void {
    if(!params) params = {};
    let modal = this.modalCtrl.create(page, params);
    modal.present();
  }

  public closeModal(): void {
    this.viewCtrl.dismiss().catch(() => {});
  }

  public showAlert(title: string, buttons: Array<any>, subTitle?: string): void {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: buttons
    });
    alert.present();
  }

  public showConfirm(title: string, message: string, buttons: Array<any>): void {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons
    });
    alert.present();
  }

  public showActionSheet(title: string, buttons: Array<any>, subTitle?: string): void {
    let actionSheet = this.actionSheetCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: buttons
    });
    actionSheet.present();
  }

  public showToast(message: string): void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  public showLoading(content: string) {
    this.loading = this.loadingCtrl.create({
      content: content
    });
    return this.loading.present();
  }

  public hideLoading(): void {
    this.loading.dismiss();
  }

  public setRootPage(page: any, params?: any): void {
    this.navCtrl.setRoot(page, params, {
      animate: true,
      animation: 'md-transition',
      direction: "forward"
    });
  }

  public goForward(page: any, params?: any): void {
    if(!params) params = {};
    this.navCtrl.push(page, params, {
      animate: true,
      animation: 'md-transition',
      direction: 'forward'
    });
  }

  public goBackward(): void {
    this.navCtrl.pop({
      animate: true,
      animation: 'md-transition',
      direction: 'back'
    });
  }

}
