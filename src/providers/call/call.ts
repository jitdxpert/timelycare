import { Injectable } from '@angular/core';
import { ModalController, ToastController } from 'ionic-angular';

// Import providers
import { State } from '../state/state';
import { ApiProvider } from '../api/api';
import { UsersProvider } from '../users/users';

//Import Pages
import { CallPage } from '../../pages/call/script';

declare var Peer: any;

@Injectable()
export class CallProvider {

  serverIp: string;
  serverPort: number;
  serverSecure: boolean;

  conn: any;
  interval: any;

  constructor(public api: ApiProvider, public user: UsersProvider, public modal: ModalController, public toastCtrl: ToastController){
    this.serverIp = 'timelycare.org';
    // this.serverIp = '192.168.43.233';
    this.serverPort = 8080;
    this.serverSecure = false;

    this.conn = null;
  }

  public allowUserMedia(): void{
    let self = this;

    self.user.getUserMediaAudioStream(() => {
      setTimeout(() => {
        self.user.stopLocalStream();
      }, 5000);
    });
  }

  public connectToPeerServer(){
    let self = this;

    let peer = State.get('peer');
    let myId = State.get('uid');

    if(myId == 0){
      return;
    }

    if(peer){
      return;
    }

    try{
      peer = new Peer(myId, {
        host: self.serverIp,
        port: self.serverPort,
        secure: self.serverSecure
      });

      State.set('peer', peer);

      peer.on('close', function(conn){
        self.showToast('Disconnected From Server');
        self.connectToPeerServer();
      });

      peer.on('disconnected', function(){
        peer.reconnect();
      });

      peer.on('connection', (_conn: any) => {
        self.conn = _conn;
      });

      peer.on('call', (call: any) => {
        let conn = self.conn;
        self.showCallModal({
          call: call,
          conn: conn,
          init: false
        });
      });

    }catch(e){}

    if(!self.interval){
      self.checkForConnectivity();
    }
  }

  public checkForConnectivity(): void{
    let self = this;
    self.interval = setInterval(() => {
      let peer = State.get('peer');
      if(!peer.id){
        self.connectToPeerServer();
      }
    }, 3000);
  }

  public showCallModal(params:any): void {
    let modal = this.modal.create(CallPage, params);
    modal.present();
  }

  public showToast(message: string): void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
