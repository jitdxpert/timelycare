import { Injectable } from '@angular/core';

// import providers
import { ApiProvider } from '../api/api';

@Injectable()
export class ChatProvider {

  constructor(public api: ApiProvider) {}

  public get(receiver_id: number, lastChatId: number, type?: number) {
    let params = 'receiverId=' + receiver_id + '&lastChatId=' + lastChatId;
    let url = 'chat-user';
    if(type == 2){
      url = 'chat-group';
    }
    return this.api.get(url, params);
  }

  public getPrevChat(receiver_id: number, firstChatId: number, type?: number){
    let params = 'receiverId=' + receiver_id + '&firstChatId=' + firstChatId;
    let url = 'chat-prev-user';
    if(type == 2){
      url = 'chat-prev-group';
    }
    return this.api.get(url, params);
  }

  public send(params: any) {
    return this.api.post('chat-save', params);
  }

  public sendAudio(targetPath: string, options: any) {
    return this.api.multipart(targetPath, 'chat-audio-save', options);
  }

  public sendMedia(targetPath: string, options: any) {
    return this.api.multipart(targetPath, 'chat-media-save', options);
  }

}
