import { Injectable } from '@angular/core';

// import providers
import { ApiProvider } from '../api/api';

@Injectable()
export class ScheduleProvider{

  constructor(public api: ApiProvider){}

  public get(date?: string){
    return this.api.get('schedule', date);
  }

  public getShifts(){
    return this.api.get('shifts');
  }

  public getRequests(){
    return this.api.get('shift-request-get');
  }

  public postRequest(params: any){
    return this.api.post('shift-request-post', params);
  }

}
