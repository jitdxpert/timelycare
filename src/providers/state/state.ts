export class State {

  public static global: Array<any> = [];

  public static set(key: string, value: any): void {
    this.global[key] = value;
  }

  public static get(key: string) {
    return this.global[key];
  }

  public static clear(): void {
    this.global = [];
  }

}
