import { Injectable } from '@angular/core';

// import providers
import { State } from '../state/state';
import { ApiProvider } from '../api/api';

@Injectable()
export class UsersProvider {

  constructor(public api: ApiProvider){}

  public getDoctors(page: number, tab: string, query?: string) {
    let params = 'p=' + page + '&t=' + tab;
    if(query) params += '&q=' + query;
    let url = (tab == 'recent' ? 'recent-doctors' : 'doctors');
    return this.api.get(url, params);
  }

  public getNurses(page: number, tab: string, query?: string) {
    let params = 'p=' + page + '&t=' + tab;
    if(query) params += '&q=' + query;
    let url = (tab == 'recent' ? 'recent-nurses' : 'nurses');
    return this.api.get(url, params);
  }

  public getUser(id?: any) {
    let params = 'id=' + id;
    if(!id){
      params = '';
    }
    return this.api.get('user', params);
  }

  public userFavorite(params: any) {
    return this.api.post('favorite', params);
  }

  public getUserChatMedia(id: any){
    let params = 'id=' + id;
    return this.api.get('chat-media', params);
  }

  public onCall(user, callback): void{
    let self = this;

    State.set('peerId', user.id);
    self.getUserMediaAudioStream(() => {
      let peer = State.get('peer');
      let peerId = State.get('peerId');
      let localStream = State.get('localStream');

      let conn = peer.connect(peerId);
      let call = peer.call(peerId, localStream);
      let params = {
        call: call,
        conn: conn,
        init: true
      };
      return callback(params);
    });
  }

  public getUserMediaAudioStream(callback){
    navigator.mediaDevices.getUserMedia({
      audio: true,
      video: false
    }).then((stream: any) => {
      State.set('localStream', stream);
      callback();
    }).catch((err: any) => {});
  }

  public stopLocalStream(): void{
    let track = null;
    let tracks = null;
    let localStream = State.get('localStream');
    if(localStream){
      tracks = localStream.getAudioTracks();
    }
    if(tracks.length){
      track = tracks[0];
    }
    if(track){
      track.stop();
    }
  }

  public playPauseStreaming(mute: boolean): void{
    let track = null;
    let tracks = null;
    let localStream = State.get('localStream');
    if(localStream){
      tracks = localStream.getAudioTracks();
    }
    if(tracks.length){
      track = tracks[0];
    }
    if(track){
      track.enabled = mute;
    }
  }

}
