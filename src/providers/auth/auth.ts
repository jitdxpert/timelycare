import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

// import providers
import { State } from '../state/state';
import { ApiProvider } from '../api/api';

@Injectable()
export class AuthProvider {

  constructor(public storage: Storage, public api: ApiProvider) {}

  public initialize() {
    return new Promise((resolve, reject) => {
      this.storage.get('user').then((user: any) => {
        if(!user) user = {};
        this.initState(user);
        resolve(user);
      }, (err: any) => {
        reject(err);
      });
    });
  }

  public initState(user: any): void {
    if(!user.mpin) user.mpin = 0;
    if(!user.uid) user.uid = 0;
    if(!user.token) user.token = 0;
    if(typeof user.type == 'undefined') user.type = -1;
    State.set('mpin', user.mpin.toString());
    State.set('uid', user.uid.toString());
    State.set('token', user.token.toString());
    State.set('type', user.type.toString());
  }

  public login(credentials: any) {
    credentials.offset = new Date().getTimezoneOffset() * 60;
    return this.api.post('login', credentials);
  }

  public changePassword(password: any) {
    return this.api.post('change-password', password);
  }

  public updateStatus(availability: any) {
    return this.api.post('availabilities-save', availability);
  }

  public getStatus() {
    return this.api.get('availabilities-get');
  }

  public profileImage(targetPath: string, options: any) {
    return this.api.multipart(targetPath, 'image', options);
  }

  public updateOnlineStatus(status: number) {
    let params = {
      status: status
    };
    return this.api.post('update-status', params);
  }

  public logout() {
    return new Promise((resolve, reject) => {
      this.storage.set('user', {});
      State.set('uid', '0');
      State.set('token', '0');
      State.set('mpin', '0');
      State.set('type', '-1');

      State.set('peer', null);
      State.set('peerId', null);
      State.set('localStream', '#');

      resolve();
    });
  }

}
