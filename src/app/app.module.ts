import { IonicStorageModule } from '@ionic/storage';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

// import native plugins
import { File } from '@ionic-native/file';
import { Media } from '@ionic-native/media';
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { StatusBar } from '@ionic-native/status-bar';
import { FileChooser } from '@ionic-native/file-chooser';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { MediaCapture } from '@ionic-native/media-capture';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

// import pages
import { MyApp } from './app.component';
import { PinPage } from '../pages/pin/script';
import { UserPage } from '../pages/user/script';
import { CallPage } from '../pages/call/script';
import { ChatPage } from '../pages/chat/script';
import { TabsPage } from '../pages/tabs/script';
import { NursePage } from '../pages/nurse/script';
import { LoginPage } from '../pages/login/script';
import { StatusPage } from '../pages/status/script';
import { DoctorPage } from '../pages/doctor/script';
import { RequestPage } from '../pages/request/script';
import { SettingsPage } from '../pages/settings/script';
import { SchedulePage } from '../pages/schedule/script';
import { PinChangePage } from '../pages/pinchange/script';
import { PassChangePage } from '../pages/passchange/script';

// import providers
import { ApiProvider } from '../providers/api/api';
import { AuthProvider } from '../providers/auth/auth';
import { CallProvider } from '../providers/call/call';
import { ChatProvider } from '../providers/chat/chat';
import { UsersProvider } from '../providers/users/users';
import { UtilityProvider } from '../providers/utility/utility';
import { ScheduleProvider } from '../providers/schedule/schedule';

// import pipes
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    MyApp,
    PinPage,
    UserPage,
    CallPage,
    ChatPage,
    TabsPage,
    LoginPage,
    NursePage,
    DoctorPage,
    StatusPage,
    RequestPage,
    SettingsPage,
    SchedulePage,
    PinChangePage,
    PassChangePage
  ],
  imports: [
    PipesModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'bottom'
    }),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PinPage,
    UserPage,
    CallPage,
    ChatPage,
    TabsPage,
    LoginPage,
    NursePage,
    DoctorPage,
    StatusPage,
    RequestPage,
    SettingsPage,
    SchedulePage,
    PinChangePage,
    PassChangePage
  ],
  providers: [
    File,
    Media,
    Camera,
    Network,
    Transfer,
    FilePath,
    StatusBar,
    ApiProvider,
    FileChooser,
    CallProvider,
    ChatProvider,
    MediaCapture,
    AuthProvider,
    SplashScreen,
    IOSFilePicker,
    UsersProvider,
    FingerprintAIO,
    UtilityProvider,
    ScheduleProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
