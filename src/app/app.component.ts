import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Component, OnInit } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Platform, AlertController } from 'ionic-angular';

// import providers
import { AuthProvider } from '../providers/auth/auth';
import { CallProvider } from '../providers/call/call';

// import pages
import { LoginPage } from '../pages/login/script';
import { PinPage } from '../pages/pin/script';

@Component({
  templateUrl: 'app.html',
  providers: [
    AuthProvider,
    CallProvider
  ]
})
export class MyApp implements OnInit {

  public rootPage:any;

  constructor(public platform: Platform, public network: Network, public statusBar: StatusBar, public splashScreen: SplashScreen, public auth: AuthProvider, public call: CallProvider, public alertCtrl: AlertController) {}

  public ngOnInit(): void {
    this.initializeApp();
  }

  public initializeApp(): void {
    this.platform.ready().then(() => {

      this.network.onDisconnect().subscribe(() => {
        let alert = this.alertCtrl.create({
          title: 'No network detected. Connect and try again.',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'Exit',
              handler: () => {
                this.platform.exitApp();
              }
            }
          ]
        });
        alert.present();
      });

      this.network.onConnect().subscribe(() => {
        console.log('network connected!');
      });

      this.platform.resume.subscribe((e) => {
        this.auth.updateOnlineStatus(1).then(() => {
          console.log('online');
        });
      });

      this.platform.pause.subscribe((e) => {
        this.auth.updateOnlineStatus(0).then(() => {
          console.log('offline');
        });
      });

      this.statusBar.styleDefault();

      this.auth.initialize().then((user: any) => {

        if(!user) user = {};
        if(!user.mpin){
          this.rootPage = LoginPage;
        }else if(user.mpin.length != 4){
          this.rootPage = LoginPage;
        }else{
          this.rootPage = PinPage;
        }

        this.call.allowUserMedia();
        this.call.connectToPeerServer();

        setTimeout(() => {
          this.splashScreen.hide();
        }, 300);
      });

    });
  }



}
