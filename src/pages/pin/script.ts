import { Platform } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

// import providers
import { State } from '../../providers/state/state';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilityProvider } from '../../providers/utility/utility';

// import pages
import { TabsPage } from '../tabs/script';
import { LoginPage } from '../login/script';

@Component({
  selector: 'page-pin',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    UtilityProvider
  ]
})
export class PinPage implements OnInit {

  public mpin: string;
  public passcode: string;
  public numberPad: Array<any>;

  constructor(public platform: Platform, public faio: FingerprintAIO, public utility: UtilityProvider, public auth: AuthProvider) {}

  public ngOnInit(): void{
    let self = this;

    self.faio.isAvailable().then((res: any) => {
      self.open();
    }).catch((err: any) => {});

    self.passcode = '';
    self.mpin = State.get('mpin');

    self.numberPad = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
      [-1, 0, 'del']
    ];
  }

  public add(value: string): void{
    if(!this.passcode){
      this.passcode = '';
    }
    this.passcode += value;
    if(this.passcode.length == 4){
      this.confirmPin();
    }
  }

  public confirmPin(): void{
    let self = this;

    if(self.mpin != self.passcode){
      self.utility.showToast('Invalid PIN');
      self.passcode = '';
      return;
    }
    self.utility.showLoading('Loading...').then(() => {
      self.auth.updateOnlineStatus(1).then((res: any) => {
        if(res.type == 'success'){
          self.utility.setRootPage(TabsPage);
        }else{
          self.utility.setRootPage(LoginPage);
        }
        self.utility.hideLoading();
      });
    });
  }

  public delete(): void{
    this.passcode = this.passcode.substr(0, this.passcode.length - 1);
  }

  public open(): void{
    let self = this;

    self.faio.show({
      clientId: 'TimelyCare',
      clientSecret: 'password',
      disableBackup: true,
      localizedFallbackTitle: 'Use PIN',
      localizedReason: 'Please authenticate'
    }).then((res: any) => {
      self.utility.showLoading('Loading...').then(() => {
        self.auth.updateOnlineStatus(1).then(() => {
          self.utility.setRootPage(TabsPage);
          self.utility.hideLoading();
        });
      });
    }).catch((err: any) => {});
  }

}
