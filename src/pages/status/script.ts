import { Component, OnInit } from '@angular/core';
import moment from 'moment';

// import providers
import { AuthProvider } from '../../providers/auth/auth';
import { UtilityProvider } from '../../providers/utility/utility';

@Component({
  selector: 'page-status',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    UtilityProvider
  ]
})
export class StatusPage implements OnInit {

  public voiceCall: boolean;
  public textMessage: boolean;
  public customMessage: string;
  public againAvailable: string;
  public minAvailable: string;
  public showSpinner: boolean;

  constructor(public auth: AuthProvider, public utility: UtilityProvider) {}

  public ngOnInit(): void {
    this.showSpinner = true;
    let now = moment.utc().local();
    this.againAvailable = moment(now.format(), moment.ISO_8601).format();
    this.minAvailable = moment(now.format(), moment.ISO_8601).format();
    this.auth.getStatus().then((res: any) => {
      if(res.type == 'success'){
        this.voiceCall = res.availabilities.voice_call;
        this.textMessage = res.availabilities.text_message;
        this.customMessage = res.availabilities.custom_message;
        if(res.availabilities.available_again){
          let newTime = moment.utc(res.availabilities.available_again).local();
          this.againAvailable = moment(newTime.format(), moment.ISO_8601).format();
        }
      }

      this.showSpinner = false;
    }, (err: any) => {
      this.showSpinner = false;
    });
  }

  public toggleStatus(value: any, field: string, fieldMsg: string): void {
    this.showSpinner = true;
    if(field == 'custom_message'){
      value = this.customMessage;
    }else if(field == 'available_again'){
      value = moment.utc(value).format('YYYY-MM-DD HH:mm:ss');
    }else{
      value = (value ? 1 : 0);
    }
    let params = {
      field: field,
      value: value
    };
    this.auth.updateStatus(params).then((res: any) => {
      this.utility.showToast(fieldMsg + ' ' + res.message);

      this.showSpinner = false;
    }, (err: any) => {
      this.showSpinner = false;
    });
  }

  public closeModal(): void {
    this.utility.closeModal();
  }

}
