import { Component, OnInit } from '@angular/core';
import moment from 'moment';

// import providers
import { UtilityProvider } from '../../providers/utility/utility';
import { ScheduleProvider } from '../../providers/schedule/schedule';

@Component({
  selector: 'page-request',
  templateUrl: 'view.html',
  providers: [
    UtilityProvider,
    ScheduleProvider
  ]
})
export class RequestPage implements OnInit {

  public isWorking: boolean;
  public shiftDate: string;
  public shiftDateMin: string;
  public showSpinner: boolean;

  constructor(public utility: UtilityProvider, public schedule: ScheduleProvider) {}

  public ngOnInit(): void {
    this.showSpinner = true;
    this.shiftDate = null;
    this.isWorking = false;
    this.utility.showLoading('Loading...').then(() => {
      let now = moment.utc().local();
      this.shiftDate = this.shiftDateMin = moment(now.format(), moment.ISO_8601).format();
      this.utility.hideLoading();
      this.showSpinner = false;
    });
  }

  public sendRequest(): void {
    this.showSpinner = true;
    this.utility.showLoading('Loading...').then(() => {
      let params = {
        date: moment.utc(this.shiftDate).format('YYYY-MM-DD'),
        is_work: (this.isWorking ? 1 : 2)
      };
      this.schedule.postRequest(params).then((res: any) => {
        this.utility.showToast(res.message);
        this.utility.closeModal();
        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.showToast(err.error.message);
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public closeModal(): void {
    this.utility.closeModal();
  }

}
