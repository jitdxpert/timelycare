import { Component, OnInit } from '@angular/core';

// import providers
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';
import { CallProvider } from '../../providers/call/call';

// import pages
import { ChatPage } from '../chat/script';
import { CallPage } from '../call/script';
import { UserPage } from '../user/script';

@Component({
  selector: 'page-doctor',
  templateUrl: 'view.html',
  providers: [
    UsersProvider,
    UtilityProvider,
    CallProvider
  ]
})
export class DoctorPage implements OnInit {

  public query: string;
  public doctors: any;
  public showSpinner: boolean;
  public imgSpinner: boolean;
  public tab: string = 'recent';
  public page: number = 1;
  public response: string;
  public message: string = '';
  public pagination: {
    page: number,
    left: number,
    right: number
  };
  private recentDoctors: Array<any>;
  private interval: any;

  constructor(public user: UsersProvider, public utility: UtilityProvider, public call: CallProvider){}

  public ngOnInit(): void{
    this.imgSpinner = true;
    this.doctors = [];
    this.showingLoading().then(() => {
      this.filterRecords(this.tab);
    });

    this.interval = setInterval(() => {
      this.filterRecords(this.tab);
    }, 10000);
  }

  public ionViewWillLeave(): void{
    clearInterval(this.interval);
  }

  public imageLoaded(): void{
    setTimeout(() => {
      this.imgSpinner = false;
    }, 1000);
  }

  public loadingDoctors(infiniteScroll: any): void{
    if(this.pagination.right > 0){
      this.page += 1;
      this.user.getDoctors(this.page, this.tab, this.query).then((res: any) => {
        for(let i in res.doctors){
          this.doctors.push(res.doctors[i]);
        }
        this.pagination = res.pagination;

        this.showSpinner = false;
      }, (err: any) => {
        this.showSpinner = false;
      });
    }

    setTimeout(() => {
      infiniteScroll.complete();
    }, 1000);
  }

  public searchDoctor(): void{
    if(this.tab == 'recent'){
      this.doctors = this.recentDoctors.filter((item) => {
        if(this.query){
          return item.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1 || item.description.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
        }else{
          return item;
        }
      });
    }else{
      this.page = 1;
      this.showSpinner = true;
      this.user.getDoctors(this.page, this.tab, this.query).then((res: any) => {
        this.doctors = res.doctors;
        this.response = res.type;
        if(this.response == 'error'){
          this.message = res.message;
        }
        this.pagination = res.pagination;

        this.showSpinner = false;
      }, (err: any) => {
        this.showSpinner = false;
      });
    }
  }

  public subTabHandler(tab: string): void{
    this.showingLoading().then(() => {
      this.filterRecords(tab);
    });
  }

  public showingLoading() {
    return this.utility.showLoading('Loading...').then(() => {
      this.showSpinner = true;
    });
  }

  public presentActionSheet(doctor: any): void{
    let buttons = [];
    if(doctor.availability == null){
      doctor.availability = {};
    }

    if(doctor.availability.voice_call == 1 || doctor.availability.text_message == 1){
      if(doctor.type == 0 && doctor.availability.voice_call == 1){
        buttons.push({
          text: 'Call',
          icon: 'icon-phone',
          handler: () => {
            this.makeCall(doctor);
          }
        });
      }

      if(doctor.availability.text_message == 1){
        buttons.push({
          text: 'Message',
          icon: 'icon-chat',
          handler: () => {
            this.goToChatbox(doctor.id, doctor.type);
            this.doctors.filter((item) => {
              if(item.id == doctor.id){
                item.message = 0;
              }
            });
          }
        });
      }

      buttons.push({
        text: 'Cancel',
        icon: 'icon-cancel',
        role: 'cancel'
      });
      this.utility.showActionSheet('Choose Action for '+doctor.name, buttons);
    }else{
      this.utility.showToast(doctor.availability.custom_message);
    }
  }

  public goToChatbox(id: number, type: number): void{
    this.utility.openModal(ChatPage, {id: id, type: type});
  }

  public makeCall(doctor: any): void{
    this.user.onCall(doctor, (params: any) => {
      this.utility.openModal(CallPage, params);
    });
  }

  public goToUser(id: number): void{
    this.utility.goForward(UserPage, {id: id});
  }

  public filterRecords(filter: string): void{
    this.page = 1;
    this.tab = filter;
    this.user.getDoctors(this.page, this.tab, this.query).then((res: any) => {
      this.doctors = res.doctors;
      if(this.tab == 'recent'){
        this.recentDoctors = res.doctors;
      }
      this.response = res.type;
      if(this.response == 'error'){
        this.message = res.message;
      }
      this.pagination = res.pagination;

      this.utility.hideLoading();
      this.showSpinner = false;
    }, (err: any) => {
      this.utility.hideLoading();
      this.showSpinner = false;
    });
  }

  public doctorFavorite(doctor): void{
    this.showSpinner = true;
    let params = {
      favId: doctor.id,
      role: doctor.type,
      flag: doctor.favorite
    };
    this.user.userFavorite(params).then((res: any) => {
      doctor.favorite = res.favorite;
      if(this.tab == 'fav'){
        let index = this.doctors.findIndex((item: any) => item.id === doctor.id);
        this.doctors.splice(index, 1);
        if(!this.doctors.length){
          this.filterRecords(this.tab);
        }
      }
      this.utility.showToast(res.message);

      this.showSpinner = false;
    }, (err: any) => {
      this.showSpinner = false;
    });
  }

}
