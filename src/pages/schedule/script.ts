import { Component, OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular';

// import providers
import { UtilityProvider } from '../../providers/utility/utility';
import { ScheduleProvider } from '../../providers/schedule/schedule';

// import pages
import { RequestPage } from '../request/script';

@Component({
  selector: 'page-schedule',
  templateUrl: 'view.html',
  providers: [
    UtilityProvider,
    ScheduleProvider
  ]
})
export class SchedulePage implements OnInit {

  public rows: any;
  public month: string;
  public year: string;
  public prev: string;
  public next: string;
  public showSpinner: boolean;
  public shifts: Array<any>;
  public mySchedules: Array<any>;
  public leaveRequests: Array<any> = [];

  constructor(public modalCtrl: ModalController, public utility: UtilityProvider, public schedule: ScheduleProvider){}

  public ngOnInit(): void{
    this.showSpinner = true;
    this.buildCalendar().then(() => {
      this.schedule.getShifts().then((res: any) => {
        this.shifts = res.shifts;
        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public buildCalendar(date?: string): any{
    return this.utility.showLoading('Loading...').then(() => {
      this.schedule.get(date).then((res: any) => {
        this.rows = res.data.calendar;
        let monthYear = res.data.year_month_text.split(',');
        this.month = monthYear[0];
        this.year = monthYear[1];
        this.prev = res.data.prev_year_month;
        this.next = res.data.next_year_month;
      });
    });
  }

  public nextMonth(): void{
    if(this.next){
      let date = 'year_month='+this.next;
      this.buildCalendar(date).then(() => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    }
  }

  public prevMonth(): void{
    if(this.prev){
      let date = 'year_month='+this.prev;
      this.buildCalendar(date).then(() => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    }
  }

  public requestModal(): void{
    this.utility.openModal(RequestPage);
  }

}
