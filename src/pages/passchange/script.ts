import { Component, OnInit } from '@angular/core';

// import providers
import { AuthProvider } from '../../providers/auth/auth';
import { UtilityProvider } from '../../providers/utility/utility';

@Component({
  selector: 'page-password',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    UtilityProvider
  ]
})
export class PassChangePage implements OnInit {

  public showSpinner: boolean;
  public current_password: string;
  public new_password: string;
  public confirm_password: string;

  constructor(public utility: UtilityProvider, public auth: AuthProvider) {}

  public ngOnInit(): void {}

  public changePassword(): void {
    this.showSpinner = true;
    let params = {
      current_password: this.current_password,
      new_password: this.new_password,
      confirm_password: this.confirm_password
    };
    this.auth.changePassword(params).then((res: any) => {
      this.utility.showToast(res.message);
      this.utility.closeModal();
      this.current_password = '';
      this.new_password = '';
      this.confirm_password = '';
      this.showSpinner = false;
    }, (err: any) => {
      this.utility.showToast(err.error.message);
      this.showSpinner = false;
    });
  }

  public closeModal(): void {
    this.utility.closeModal();
  }

}
