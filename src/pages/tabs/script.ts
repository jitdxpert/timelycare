import { Component } from '@angular/core';

// import pages
import { DoctorPage } from '../doctor/script';
import { NursePage } from '../nurse/script';
import { SchedulePage } from '../schedule/script';
import { SettingsPage } from '../settings/script';

@Component({
  templateUrl: 'view.html',
})
export class TabsPage {

  tab1Root = DoctorPage;
  tab2Root = NursePage;
  tab3Root = SchedulePage;
  tab4Root = SettingsPage;

  constructor() {}

}
