import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';

// import providers
import { State } from '../../providers/state/state';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilityProvider } from '../../providers/utility/utility';

// import pages
import { TabsPage } from '../tabs/script';

@Component({
  selector: 'page-pinchange',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    UtilityProvider
  ]
})
export class PinChangePage implements OnInit {

  public title: string;
  public mpin: string;
  public oldPin: string;
  public passcode: string;
  public numberPad: Array<any>;
  public showSpinner: boolean;

  constructor(public utility: UtilityProvider, public storage: Storage, public auth: AuthProvider) {}

  public ngOnInit(): void {
    this.passcode = '';

    this.oldPin = this.mpin = State.get('mpin');
    if(this.mpin.length == 4){
      this.title = 'Current PIN';
    }else{
      this.title = 'Enter PIN';
    }

    this.numberPad = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
      [-1, 0, 'del']
    ];

    this.showSpinner = false;
  }

  public add(value: string): void {
    if(!this.passcode){
      this.passcode = '';
    }
    this.passcode += value;
    if(this.passcode.length == 4){
      switch(this.title){
        case 'Enter PIN':
          this.enterPin();
        break;
        case 'Confirm PIN':
          this.confirmPin();
        break;
        case 'Current PIN':
          this.currentPin();
        break;
      }
    }
  }

  public enterPin(): void {
    this.mpin = this.passcode;
    this.title = 'Confirm PIN';
    this.passcode = '';
  }

  public confirmPin(): void {
    if(this.mpin != this.passcode){
      this.utility.showToast('Invalid Confirm PIN');
      this.passcode = '';
      return;
    }

    this.mpin = this.passcode;
    State.set('mpin', this.mpin);
    let user = {
      mpin: this.mpin,
      uid: State.get('uid'),
      token: State.get('token'),
      type: State.get('type'),
    };
    this.storage.set('user', user);
    if(this.oldPin.length == 4){
      this.closeModal();
    }else{
      this.utility.setRootPage(TabsPage);
    }
  }

  public currentPin(): void {
    if(this.mpin != this.passcode){
      this.utility.showToast('Invalid Current PIN');
      this.passcode = '';
      return;
    }

    this.title = 'Enter PIN';
    this.passcode = '';
  }

  public delete(): void {
    this.passcode = this.passcode.substr(0, this.passcode.length - 1);
  }

  public closeModal(): void {
    this.utility.closeModal();
  }

  public skipModal(): void {
    this.utility.setRootPage(TabsPage);
  }

}
