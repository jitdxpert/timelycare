import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';

// import providers
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';
import { CallProvider } from '../../providers/call/call';

// import pages
import { ChatPage } from '../chat/script';
import { CallPage } from '../call/script';

@Component({
  selector: 'page-user',
  templateUrl: 'view.html',
  providers: [
    UsersProvider,
    UtilityProvider,
    CallProvider
  ]
})
export class UserPage implements OnInit {

  public id: number;
  public person: any;
  public medias: any;
  public tabBarElement: any;
  public imgSpinner: boolean;
  public showSpinner: boolean;

  constructor(public navParams: NavParams, public user: UsersProvider, public utility: UtilityProvider, public call: CallProvider) {}

  public ngOnInit(): void{
    let self = this;

    self.imgSpinner = true;
    self.showSpinner = true;
    self.medias = [];
    self.person = {};
    self.id = self.navParams.get('id');

    self.utility.showLoading('Loading...').then(() => {
      self.user.getUser(self.id).then((res: any) => {
        self.person = res.user;
        if(!this.person.description){
          this.person.description = {};
        }
        if(this.person.description){
          this.person.info = this.person.description.name;
        }

        self.user.getUserChatMedia(self.id).then((res: any) => {
          self.medias = res.medias;

          self.utility.hideLoading();
          self.showSpinner = false;
        }, (err: any) => {
          self.utility.hideLoading();
          self.showSpinner = false;
        });

      }, (err: any) => {
        self.utility.hideLoading();
        self.showSpinner = false;
      });
    });

    self.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  public goToChatbox(id: number, type: number): void{
    this.utility.openModal(ChatPage, {id: id, type: type});
  }

  public makeCall(person: any): void{
    this.user.onCall(person, (params: any) => {
      this.utility.openModal(CallPage, params);
    });
  }

  public imageLoaded(): void{
    let self = this;

    setTimeout(() => {
      self.imgSpinner = false;
    }, 1000);
  }

  public ionViewWillEnter(): void{
    this.tabBarElement.style.display = 'none';
  }

  public ionViewWillLeave(): void{
    this.tabBarElement.style.display = 'flex';
  }

  public goBackToList(): void{
    this.utility.goBackward();
  }

}
