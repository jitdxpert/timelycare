import { Component, OnInit } from '@angular/core';

// import providers
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';
import { CallProvider } from '../../providers/call/call';

// import pages
import { ChatPage } from '../chat/script';
import { CallPage } from '../call/script';
import { UserPage } from '../user/script';

@Component({
  selector: 'page-nurse',
  templateUrl: 'view.html',
  providers: [
    UsersProvider,
    UtilityProvider,
    CallProvider
  ]
})
export class NursePage implements OnInit {

  public query: string;
  public nurses: Array<any>;
  public showSpinner: boolean;
  public imgSpinner: boolean;
  public tab: string = 'recent';
  public page: number = 1;
  public response: string;
  public message: string = '';
  public pagination: {
    page: number,
    left: number,
    right: number
  };
  private recentNurses: Array<any>;
  private interval: any;

  constructor(public user: UsersProvider, public utility: UtilityProvider, public call: CallProvider) {}

  public ngOnInit(): void {
    this.imgSpinner = true;
    this.nurses = [];
    this.showingLoading().then(() => {
      this.filterRecords(this.tab);
    });

    this.interval = setInterval(() => {
      this.filterRecords(this.tab);
    }, 10000);
  }

  public ionViewWillLeave(): void {
    clearInterval(this.interval);
  }

  public imageLoaded(): void {
    setTimeout(() => {
      this.imgSpinner = false;
    }, 1000);
  }

  public loadingNurses(infiniteScroll: any): void {
    if(this.pagination.right > 0){
      this.page += 1;
      this.user.getNurses(this.page, this.tab, this.query).then((res: any) => {
        for(let i in res.nurses){
          this.nurses.push(res.nurses[i]);
        }
        this.pagination = res.pagination;

        this.showSpinner = false;
      }, (err: any) => {
        this.showSpinner = false;
      });
    }

    setTimeout(() => {
      infiniteScroll.complete();
    }, 500);
  }

  public searchNurse(): void {
    if(this.tab == 'recent'){
      this.nurses = this.recentNurses.filter((item) => {
        if(this.query){
          return item.name.toLowerCase().indexOf(this.query.toLowerCase()) > -1 || item.description.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
        }else{
          return item;
        }
      });
    }else{
      this.page = 1;
      this.showSpinner = true;
      this.user.getNurses(this.page, this.tab, this.query).then((res: any) => {
        this.nurses = res.nurses;
        this.response = res.type;
        if(this.response == 'error'){
          this.message = res.message;
        }
        this.pagination = res.pagination;

        this.showSpinner = false;
      }, (err: any) => {
        this.showSpinner = false;
      });
    }
  }

  public subTabHandler(tab: string): void {
    this.showingLoading().then(() => {
      this.filterRecords(tab);
    });
  }

  public showingLoading() {
    return this.utility.showLoading('Loading...').then(() => {
      this.showSpinner = true;
    });
  }

  public presentActionSheet(nurse: any): void {
    let buttons = [];
    if(nurse.availability == null){
      nurse.availability = {};
    }

    if(nurse.availability.voice_call == 1 || nurse.availability.text_message == 1){
      if(nurse.type == 1 && nurse.availability.voice_call == 1){
        buttons.push({
          text: 'Call',
          icon: 'icon-phone',
          handler: () => {
            this.makeCall(nurse);
          }
        });
      }

      if(nurse.availability.text_message == 1){
        buttons.push({
          text: 'Message',
          icon: 'icon-chat',
          handler: () => {
            this.goToChatbox(nurse.id, nurse.type);
            this.nurses.filter((item) => {
              if(item.id == nurse.id){
                item.message = 0;
              }
            });
          }
        });
      }

      buttons.push({
        text: 'Cancel',
        icon: 'icon-cancel',
        role: 'cancel'
      });
      this.utility.showActionSheet('Choose Action for '+nurse.name, buttons);
    }else{
      this.utility.showToast(nurse.availability.custom_message);
    }
  }

  public goToChatbox(id: number, type: number): void {
    this.utility.openModal(ChatPage, {id: id, type: type});
  }

  public makeCall(nurse: any): void {
    this.user.onCall(nurse, (params: any) => {
      this.utility.openModal(CallPage, params);
    });
  }

  public goToUser(id: number): void {
    this.utility.goForward(UserPage, {id: id});
  }

  public filterRecords(filter: string): void {
    this.page = 1;
    this.tab = filter;
    this.user.getNurses(this.page, this.tab, this.query).then((res: any) => {
      this.nurses = res.nurses;
      if(this.tab == 'recent'){
        this.recentNurses = res.nurses;
      }
      this.response = res.type;
      if(this.response == 'error'){
        this.message = res.message;
      }
      this.pagination = res.pagination;

      this.utility.hideLoading();
      this.showSpinner = false;
    }, (err: any) => {
      this.utility.hideLoading();
      this.showSpinner = false;
    });
  }

  public nurseFavorite(nurse): void {
    this.showSpinner = true;
    let params = {
      favId: nurse.id,
      role: nurse.type,
      flag: nurse.favorite
    };
    this.user.userFavorite(params).then((res: any) => {
      nurse.favorite = res.favorite;
      if(this.tab == 'fav'){
        let index = this.nurses.findIndex((item: any) => item.id === nurse.id);
        this.nurses.splice(index, 1);
        if(!this.nurses.length){
          this.filterRecords(this.tab);
        }
      }
      this.utility.showToast(res.message);

      this.showSpinner = false;
    }, (err: any) => {
      this.showSpinner = false;
    });
  }

}
