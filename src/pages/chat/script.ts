import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { MediaCapture } from '@ionic-native/media-capture';
import { Platform, NavParams, Content } from 'ionic-angular';
import { Component, OnInit, ViewChild, NgZone } from '@angular/core';

// import providers
import { State } from '../../providers/state/state';
import { ChatProvider } from '../../providers/chat/chat';
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';

// import pages
import { UserPage } from '../user/script';

declare let cordova: any;

@Component({
  selector: 'page-chat',
  templateUrl: 'view.html',
  providers: [
    ChatProvider,
    UsersProvider,
    UtilityProvider
  ]
})
export class ChatPage implements OnInit {

  public id: number;
  public type: number;
  public enter: boolean;
  public imgSpinner: boolean;
  public userId: number;
  public firstChatId: number;
  public lastChatId: number;
  public person: any = {};
  public chatText: string;
  public mediaUrl: string;
  public chatMessages: Array<any>;
  public tabBarElement: any;
  public interval: any;
  public onfocus: boolean;
  public lastImage: string;
  public showSpinner: boolean;
  public lastScrollTop: number;
  public timerLoadChat: any;
  public infiniteScroll: boolean;

  @ViewChild(Content) content: Content;

  constructor(public zone: NgZone, public platform: Platform, public navParams: NavParams, public chat: ChatProvider, public user: UsersProvider, public utility: UtilityProvider, public mediaCapture: MediaCapture, public filePath: FilePath, public camera: Camera, public file: File) {}

  public ngOnInit(): void{
    this.imgSpinner = true;
    this.showSpinner = true;

    this.onfocus = false;
    this.enter = true;

    this.lastChatId = 0;
    this.infiniteScroll = false;
    this.chatMessages = [];

    this.userId = State.get('uid');
    this.id = this.navParams.get('id');
    this.type = this.navParams.get('type');
    if(this.type != 2 && this.type != State.get('type')){
      this.type = 3;
    }

    this.utility.showLoading('Loading...').then(() => {
      this.user.getUser(this.id).then((res: any) => {
        this.person = res.user;
        this.fetchChatData();

        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });

    this.pullMessages();

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  public pullMessages(): void{
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      this.fetchChatData();
    }, 10000);
  }

  public objectLoaded(): void{
    setTimeout(() => {
      this.imgSpinner = false;
    }, 1000);
  }

  public loadingPrevChats(refresher: any): void{
    if(this.chatMessages.length < 5){
      refresher.complete();
      return;
    }
    this.firstChatId = this.chatMessages[0].id;
    this.chat.getPrevChat(this.id, this.firstChatId, this.type).then((res: any) => {
      if(!res.chats){
        refresher.complete();
        return;
      }
      this.imgSpinner = true;
      var chatLength = res.chats.length;
      for(let i = chatLength;i > 0;i--){
        this.chatMessages.unshift(res.chats[chatLength - i]);
      }

      this.infiniteScroll = false;
      this.content.scrollTo(0, 20, 0);
      setTimeout(() => {
        this.chatMessages = this.chatMessages;
        refresher.complete();
      }, 500);
    }, (err: any) => {
      refresher.complete();
    });
  }

  public fetchChatData(): void{
    let lastChat;
    this.chat.get(this.id, this.lastChatId, this.type).then((res: any) => {
      if(!res.chats){
        return this.showEmtyMessage();
      }
      for(let i = res.chats.length; i > 1; i--){
        lastChat = res.chats[i-1];
        this.chatMessages.push(lastChat);
      }
      lastChat = res.chats[0];
      this.onMessageAdded(lastChat);

      this.showSpinner = false;
    }, (err: any) => {
      this.showSpinner = false;
    });
  }

  public showEmtyMessage(): void{}

  public ionViewWillEnter(): void{
    if(!this.tabBarElement) return;
    this.tabBarElement.style.display = 'none';
  }

  public ionViewWillLeave(): void{
    clearInterval(this.interval);
    this.tabBarElement.style.display = 'flex';
  }

  public sendAttachment(): void{
    let buttons = [
      {
        text: 'Open Gallery',
        icon: 'icon-image',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Take Photo',
        icon: 'icon-camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        icon: 'icon-cancel',
        role: 'cancel'
      }
    ];
    this.utility.showActionSheet('Choose Photo Source', buttons);
  }

  public takePicture(sourceType: any): void{
    let options = {
      quality: 100,
      sourceType: sourceType,
      allowEdit: false,
      saveToPhotoAlbum: false,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation:true
    };

    this.camera.getPicture(options).then((imagePath: string) => {
      if(this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY){
        this.filePath.resolveNativePath(imagePath).then((filePath: string) => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
      }else{
        let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err: any) => {});
  }

  public createFileName(): string{
    let d = new Date(),
    n = d.getTime(),
    newFileName = n + ".jpg";
    return newFileName;
  }

  public copyFileToLocalDir(namePath: string, currentName: string, newFileName: string): void{
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then((res: any) => {
      this.lastImage = newFileName;
      this.uploadImage();
    }, (err: any) => {});
  }

  public pathForImage(img: string) {
    if(img === null){
      return '';
    }else{
      return cordova.file.dataDirectory + img;
    }
  }

  public sendMessage(): void{
    if(this.chatText && this.enter){
      this.enter = false;
      let params = {
        receiverId: this.person.id,
        chatType: this.person.type,
        message: this.chatText
      };
      this.chatText = '';
      clearInterval(this.interval);
      this.chat.send(params).then((res: any) => {
        this.onMessageAdded(res.lastChat);

        this.enter = true;
        this.showSpinner = false;
      }, (err: any) => {
        this.showSpinner = false;
      });
    }
  }

  public captureAudio(): void{
    this.mediaCapture.captureAudio().then((res: any) => {
      this.sendAudioFile(res);
    }, (err: any) => {});
  }

  public sendAudioFile(files: any): void{
    let targetPath = files[0].fullPath;
    let filename = files[0].name;
    let options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {
        fileName: filename,
        receiverId: this.person.id,
        chatType: this.person.type
      }
    };

    clearInterval(this.interval);
    this.showSpinner = true;
    this.utility.showLoading('Loading...').then(() => {
      this.chat.sendAudio(targetPath, options).then((res: any) => {
        try{
          res = JSON.parse(res);
        }catch(e){
          res = {};
        }
        this.chatText = '';

        this.onMessageAdded(res.lastChat);

        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public uploadImage(): void{
    let targetPath = this.pathForImage(this.lastImage);
    let filename = this.lastImage;
    let options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {
        fileName: filename,
        receiverId: this.person.id,
        chatType: this.person.type
      }
    };

    clearInterval(this.interval);
    this.showSpinner = true;
    this.utility.showLoading('Loading...').then(() => {
      this.chat.sendMedia(targetPath, options).then((res: any) => {
        try{
          res = JSON.parse(res);
        }catch(e){
          res = {};
        }
        this.chatText = '';
        this.onMessageAdded(res.lastChat);

        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public onMessageAdded(lastChat: any): void{
    this.pullMessages();
    this.lastChatId = lastChat.id;
    this.chatMessages.push(lastChat);
    this.imgSpinner = false;
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 500);
  }

  public onEnter(keyCode: number): void{
    if(keyCode === 13){
      this.sendMessage();
    }
  }

  public onFocusIn(): void{
    this.onfocus = true;
  }

  public onFocusOut(): void{
    setTimeout(() => {
      this.onfocus = false;
    }, 500);
  }

  public goToUser(id: number): void{
    this.utility.goForward(UserPage, {id: id});
  }

  public goBackToList(): void{
    this.utility.goBackward();
  }

}
