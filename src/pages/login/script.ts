import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

// import providers
import { State } from '../../providers/state/state';
import { AuthProvider } from '../../providers/auth/auth';
import { CallProvider } from '../../providers/call/call';
import { UtilityProvider } from '../../providers/utility/utility';

// import pages
import { TabsPage } from '../tabs/script';
import { PinChangePage } from '../pinchange/script';

@Component({
  selector: 'page-login',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    CallProvider,
    UtilityProvider
  ]
})
export class LoginPage implements OnInit{

  public type: string = 'password';
  public showPass: boolean = false;
  public email: string;
  public password: string;

  constructor(public auth: AuthProvider, public call: CallProvider, public utility: UtilityProvider, public storage: Storage){}

  public ngOnInit(): void{
    this.storage.set('user', {});
  }

  public onLogin(): void{
    this.utility.showLoading('Authenticating...').then(() => {
      let credentials = {
        email: this.email,
        password: this.password
      };
      this.auth.login(credentials).then((res: any) => {
        let user = {
          mpin: State.get('mpin'),
          uid: res.user.id,
          token: res.user.token,
          type: res.user.type
        };
        this.auth.initState(user);
        this.storage.set('user', user);
        this.call.connectToPeerServer();
        if(user.mpin.length == 4){
          this.utility.setRootPage(TabsPage);
        }else{
          this.utility.setRootPage(PinChangePage);
        }
        this.utility.hideLoading();
      }, (err: any) => {
        this.utility.hideLoading();
        this.utility.showToast(err.error.message);
      });
    });
  }

  public showPassword(): void{
    this.showPass = !this.showPass;
    if(this.showPass){
      this.type = 'text';
    }else{
      this.type = 'password';
    }
  }

}
