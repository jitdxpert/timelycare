import { Platform, NavParams } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

// import providers
import { State } from '../../providers/state/state';
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';

@Component({
  selector: 'page-call',
  templateUrl: 'view.html',
  providers: [
    UsersProvider,
    UtilityProvider
  ]
})
export class CallPage implements OnInit {

  public initiator: boolean;

  public person: any;
  public personId: number;

  public call: any;
  public conn: any;
  public remoteAudio: string;

  public sec: number;
  public min: number;
  public duration: string;
  public message: string;

  public accept: boolean;
  public mute: boolean;
  public speaker: boolean;

  public summery: any;
  public unavail: any;

  public audioToggle: any;

  constructor(public platform: Platform, public navParams: NavParams, public sanitizer: DomSanitizer, public utility: UtilityProvider, public user: UsersProvider){
    let self = this;
    self.platform.ready().then(() => {
      self.audioToggle = (<any>window).AudioToggle;
    });
  }

  public ngOnInit(): void{
    let self = this;

    self.person = {};
    self.person.image = 'assets/imgs/avatar-user.png';

    self.duration = null;
    self.sec = 0;
    self.min = 0;

    self.accept = false;
    self.mute = false;
    self.speaker = false;

    self.call = this.navParams.get('call');
    self.conn = this.navParams.get('conn');
    self.initiator = self.navParams.get('init');

    self.personId = self.call.peer;

    if(self.initiator){
      self.message = 'Ringing...';
      setTimeout(() => {
        self.remoteAudio = 'assets/audio/dialring.mp3';
        self.audioToggle.setAudioMode(self.audioToggle.EARPIECE);
      }, 1000);
    }else{
      self.message = 'Calling...';
      setTimeout(() => {
        self.remoteAudio = 'assets/audio/callring.mp3';
        self.audioToggle.setAudioMode(self.audioToggle.SPEAKER);
      }, 1000);
    }

    self.utility.showLoading('Loading...').then(() => {
      self.user.getUser(self.personId).then((res: any) => {
        self.person = res.user;
        self.utility.hideLoading();
      }, (err: any) => {
        self.utility.hideLoading();
      });
    });

    self.call.on('stream', (stream: any) => {
      self.remoteAudio = '#';
      self.onStream(stream);
    });

    self.call.on('close', () => {
      self.onCallEnded();
    });

    self.conn.on('data', (data: any) => {
      self.onMessage(data);
    });

    self.conn.on('close', () => {
      self.onCallEnded();
    });

    self.summery = setInterval(() => {
      self.callSummery();
    }, 1000);

    self.unavail = setTimeout(() => {
      self.onCallEnded();
    }, 60000);
  }

  public onStream(stream: any): void{
    this.accept = true;
    this.message = 'Connected';
    clearTimeout(this.unavail);
    this.remoteAudio = URL.createObjectURL(stream);
  }

  public onCallEnded(): void{
    this.call.close();
    this.conn.send('reject');
    this.user.stopLocalStream();
    clearTimeout(this.unavail);
    clearInterval(this.summery);
    this.utility.closeModal();
  }

  public onMessage(msg: any): void{
    if(msg === 'reject'){
      this.onCallEnded();
    }
  }

  public callAccept(): void{
    let self = this;
    self.changeMuteMode();
    self.changeAudioMode(self.speaker);
    self.user.getUserMediaAudioStream(() => {
      let localStream = State.get('localStream');
      self.call.answer(localStream);
    });
  }

  public callReject(): void{
    this.onCallEnded();
  }

  public callMuteMode(): void{
    this.mute = !this.mute;
    if(this.accept){
      this.changeMuteMode();
    }
  }

  public changeMuteMode(): void{
    if(this.mute){
      this.user.playPauseStreaming(false);
    }else{
      this.user.playPauseStreaming(true);
    }
  }

  public callSpeakerMode(): void{
    this.speaker = !this.speaker;
    if(this.accept){
      this.changeAudioMode(this.speaker);
    }
  }

  public changeAudioMode(speaker: boolean): void{
    let self = this;
    if(speaker){
      self.audioToggle.setAudioMode(self.audioToggle.SPEAKER);
    }else{
      self.audioToggle.setAudioMode(self.audioToggle.EARPIECE);
    }
  }

  public sanitize(url: any){
    if(!url) return '#';
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  public callSummery(): void{
    if(this.accept){
      if(++this.sec === 60){
        this.sec = 0;
        if(++this.min === 60){
          this.min = 0;
        }
      }
      this.duration = (this.min < 10 ? "0" + this.min : this.min) + ":" + (this.sec < 10 ? "0" + this.sec : this.sec);
    }
  }

}
