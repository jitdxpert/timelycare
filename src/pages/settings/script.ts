import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { Component, OnInit } from '@angular/core';
import { FilePath } from '@ionic-native/file-path';
import { App, Platform } from 'ionic-angular';

// import providers
import { AuthProvider } from '../../providers/auth/auth';
import { UsersProvider } from '../../providers/users/users';
import { UtilityProvider } from '../../providers/utility/utility';

// import pages
import { LoginPage } from '../login/script';
import { StatusPage } from '../status/script';
import { PassChangePage } from '../passchange/script';
import { PinChangePage } from '../pinchange/script';

declare let cordova: any;

@Component({
  selector: 'page-settings',
  templateUrl: 'view.html',
  providers: [
    AuthProvider,
    UsersProvider,
    UtilityProvider
  ]
})
export class SettingsPage implements OnInit {

  public userData: any = {};
  public lastImage: string;
  public imgSpinner: boolean;
  public showSpinner: boolean;

  constructor(public app: App, public platform: Platform, public camera: Camera, public file: File, public filePath: FilePath, public auth: AuthProvider, public user: UsersProvider, public utility: UtilityProvider) {}

  public ngOnInit(): void {
    this.imgSpinner = true;
    this.showSpinner = true;
    this.utility.showLoading('Loading...').then(() => {
      this.user.getUser().then((res: any) => {
        this.userData = res.user;
        if(!this.userData.description){
          this.userData.description = {};
        }
        if(this.userData.description){
          this.userData.info = this.userData.description.name;
        }

        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public imageLoaded(): void {
    setTimeout(() => {
      this.imgSpinner = false;
    }, 1000);
  }

  public presentActionSheet(): void {
    let buttons = [
      {
        text: 'Open Gallery',
        icon: 'icon-image',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Take Photo',
        icon: 'icon-camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        icon: 'icon-cancel',
        role: 'cancel'
      }
    ];
    this.utility.showActionSheet('Choose Photo Source', buttons);
  }

  public takePicture(sourceType: any): void {
    let options = {
      quality: 100,
      sourceType: sourceType,
      allowEdit: true,
      saveToPhotoAlbum: false,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation:true
    };

    this.camera.getPicture(options).then((imagePath: string) => {
      if(this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY){
        this.filePath.resolveNativePath(imagePath).then((filePath: string) => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
      }else{
        let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err: any) => {
      this.utility.showToast('Error while selecting photo');
    });
  }

  public createFileName(): string {
    let d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }

  public copyFileToLocalDir(namePath: string, currentName: string, newFileName: string): void {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then((res: any) => {
      this.lastImage = newFileName;
      this.uploadImage();
    }, (err: any) => {
      this.utility.showToast('Error while storing photo');
    });
  }

  public pathForImage(img: string) {
    if(img === null){
      return '';
    }else{
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage(): void {
    let targetPath = this.pathForImage(this.lastImage);
    let filename = this.lastImage;
    let options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };

    this.utility.showLoading('Loading...').then(() => {
      this.showSpinner = true;
      this.auth.profileImage(targetPath, options).then((res: any) => {
        let response = JSON.parse(res.response);
        this.userData.image = response.image;
        this.utility.showToast(response.message);

        this.utility.hideLoading();
        this.showSpinner = false;
      }, (err: any) => {
        this.utility.showToast(err.error.message);
        this.utility.hideLoading();
        this.showSpinner = false;
      });
    });
  }

  public goToStatus(): void {
    this.utility.openModal(StatusPage);
  }

  public goToPassword(): void {
    this.utility.openModal(PassChangePage);
  }

  public goToPIN(): void {
    this.utility.openModal(PinChangePage);
  }

  public onLogout(): void {
    let buttons = [
      {
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'OK',
        handler: () => {
          this.utility.showLoading('Loading...').then(() => {
            this.auth.updateOnlineStatus(0).then(() => {
              this.auth.logout().then(() => {
                this.app.getRootNav().setRoot(LoginPage);
                this.utility.hideLoading();
              });
            });
          });
        }
      }
    ]
    this.utility.showConfirm('Confirm Logout', 'Are you sure you want to logout?', buttons);
  }

}
