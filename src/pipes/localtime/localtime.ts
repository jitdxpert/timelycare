import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'localtime',
})
export class LocaltimePipe implements PipeTransform {

   transform(value: string, ...args) {
     if(args[0] == 'chat'){
       return this.chatTime(value);
     }
     if(args[0] == 'shiftDate'){
       return this.shiftDate(value);
     }
     if(args[0] == 'shiftTime'){
       return this.shiftTime(value);
     }
   }

   public chatTime(value) {
     let date = moment.utc(value).local();

     let isCurDay = date.isSame(new Date(), "day");
     if(isCurDay){
       return moment.utc(value).local().format('LT');
     }

     let isCurMonth = date.isSame(new Date(), "month");
     let isCurYear = date.isSame(new Date(), "year");
     if(isCurMonth || isCurYear){
       return moment.utc(value).local().format('LT, Do MMM');
     }

     return moment.utc(value).local().format('LT, Do MMM, YYYY');
   }

   public shiftDate(value) {
     return moment.utc(value).local().format('Do MMM, YYYY');
   }

   public shiftTime(value) {
     return moment.utc(value).local().format('LT');
   }

}
