import { NgModule } from '@angular/core';
import { LocaltimePipe } from './localtime/localtime';
@NgModule({
	declarations: [LocaltimePipe],
	imports: [],
	exports: [LocaltimePipe]
})
export class PipesModule {}
